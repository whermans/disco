import discord

import message_counter
from configuration import get_token

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)
GUILD = 746036746751574098


@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')
    guild = client.get_guild(GUILD)
    tree = discord.app_commands.CommandTree(client)
    tree.copy_global_to(guild=guild)

    print(guild.id)


@client.event
async def on_message(message):
    author = message.author
    if not message_counter.exists(author):
        message_counter.add(author)
    else:
        message_counter.update(author)
    if message.author == client.user:
        return

    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')
    message_counter.report()

client.run(get_token())
